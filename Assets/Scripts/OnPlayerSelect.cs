﻿using System;
using UnityEngine;

namespace selectPlayer
{
    public class OnPlayerSelect : MonoBehaviour
    {
        private Action _onSelectAction;

        public void InitData(Action onSelectAction)
        {
            _onSelectAction = onSelectAction;
        }
        
        public void Select()
        {
            _onSelectAction?.Invoke();
        }
    }
}