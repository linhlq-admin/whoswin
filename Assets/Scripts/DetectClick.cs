﻿using System;
using UnityEngine;

namespace selectPlayer
{
    public class DetectClick : MonoBehaviour
    {
        private readonly RaycastHit2D[] _hits = new RaycastHit2D[32];

        private void Update()
        {
            MouseForTest();
        }

        private void MouseForTest()
        {
            if (!Input.GetMouseButtonDown(0)) return;
            if (Camera.main == null) return;
            var main = Camera.main;
            var mousePos = (Vector2) main.ScreenToWorldPoint(Input.mousePosition);

            Array.Clear(_hits, 0, _hits.Length);
            var numbHit = Physics2D.RaycastNonAlloc(mousePos, main.transform.forward, _hits,
                50f, LayerMask.GetMask("Touchable"));
            if (numbHit > 0)
            {    
                DetectHitController();
            }
        }

        private void DetectHitController()
        {
            foreach (var hit in _hits)
            {
                OnPlayerSelect classPlayerSelect = hit.transform.GetComponent<OnPlayerSelect>();
                if (classPlayerSelect != null )
                {
                    classPlayerSelect.Select();
                }
                return;
            }
        }
    }
}