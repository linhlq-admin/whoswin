﻿using System.Collections;
using selectPlayer;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class GameManage : MonoBehaviour
{
    public GameObject playerWin;
    public GameObject playerLose;

    public PlayableDirector timeLine1;
    public PlayableDirector timeLine2;

    public Button fightButton;
   
    public DetectClick detectClick;

    private bool _isUserSelectWeapon = false;

    private void Start()
    {
        fightButton.onClick.AddListener(OnClickFight);
        StoryFlow();
    }

    private void StoryFlow()
    {
        SetUpStoryMode();
   
    }

    private void SetUpStoryMode()
    {
        StartCoroutine(StoryFight());
    }

    private IEnumerator StoryFight()
    {
        timeLine1.Play();
        yield return new WaitForSeconds(5f);
        timeLine1.Stop();
        StartChoiceStory();
    }

    private void StartChoiceStory()
    {
        SetUpChoiceStory();
    }

    private void SetUpChoiceStory()
    {
        playerLose.GetComponent<OnPlayerSelect>().InitData(OnSelectPlayerLose);
        playerWin.GetComponent<OnPlayerSelect>().InitData(OnSelectPlayerWin);
    }

    private void OnSelectPlayerWin()
    {
        Debug.Log("Select True");
        _isUserSelectWeapon = true;
        // lưu trữ lựa chọn của người chơi để tính điểm
        fightButton.gameObject.SetActive(true);
    }

    private void OnSelectPlayerLose()
    {
        Debug.Log("Select False");
        _isUserSelectWeapon = false;
        // lưu trữ lựa chọn của người chơi để tính điểm
        fightButton.gameObject.SetActive(true);
    }

    private void OnClickFight()
    {
        // không để người chơi bấm được vào player nữa
        detectClick.enabled = false;
        fightButton.gameObject.SetActive(false);
        StartCoroutine(RealFight());
    }

    private IEnumerator RealFight()
    {
        timeLine2.Play();
        yield return new WaitForSeconds(5f);
        EndRealFight();
    }

    private void EndRealFight()
    {
        
        if (_isUserSelectWeapon)
        {
            // tính điểm theo weapon
        }
        else
        {
            // tính điểm theo empty
        }
    }
}
